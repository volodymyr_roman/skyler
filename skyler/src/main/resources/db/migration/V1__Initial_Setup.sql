CREATE TABLE `s_value_obj` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `s_value` longblob NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `s_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `s_email` varchar(45) NOT NULL,
  `s_enabled` bit(1) DEFAULT NULL,
  `s_first_name` varchar(255) DEFAULT NULL,
  `s_last_name` varchar(255) DEFAULT NULL,
  `s_password` varchar(60) NOT NULL,
  `s_phone_number` varchar(255) DEFAULT NULL,
  `s_recovery_email` varchar(45) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_lswi2jh06hqlmk4ceucakmux0` (`s_email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `s_user_roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `s_role` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `s_server_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `s_protocol_version_major` varchar(255) DEFAULT NULL,
  `s_protocol_version_minor` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `s_sensor_state` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `s_value` longblob NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `s_sensor_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `s_hardware_id` varchar(255) DEFAULT NULL,
  `s_hardware_spec` longblob NOT NULL,
  `s_hardware_type` varchar(255) DEFAULT NULL,
  `s_unique_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `s_sensor_event` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `s_timestamp` date NOT NULL,
  `s_value` longblob NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_2c6ed4paxmv3b0y7efwr9c4ue` (`s_timestamp`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `s_sensor` (
  `id` int(11) NOT NULL,
  `s_sensor_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `s_sa_system_event` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `s_state` varchar(255) DEFAULT NULL,
  `s_timestamp` date NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_2e6dq169wvcyudtbr5m6ndslv` (`s_timestamp`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `s_sa_configuration` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `s_configuration_value` longblob NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `s_persistent_logins` (
  `series` varchar(255) NOT NULL,
  `s_email` varchar(255) NOT NULL,
  `s_last_used` datetime DEFAULT NULL,
  `s_token` varchar(255) NOT NULL,
  PRIMARY KEY (`series`),
  UNIQUE KEY `UK_2bapff65u7nb2om4mynum5jsl` (`s_email`),
  UNIQUE KEY `UK_fg20gj3wab4x4sne0jdfteq0f` (`s_token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `s_device` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `s_device_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `s_gateway_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `s_firmware_version` varchar(255) DEFAULT NULL,
  `s_gateway_ip` varchar(255) DEFAULT NULL,
  `s_unique_id` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `s_gateways` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `s_gateway_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `s_actuator_state` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `s_value` longblob NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `s_actuator_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `s_hardware_id` varchar(255) DEFAULT NULL,
  `s_hardware_spec` longblob NOT NULL,
  `s_hardware_type` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `s_actuator` (
  `id` int(11) NOT NULL,
  `s_actuator_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `s_joined_device_actuators` (
  `s_gateway_fk` int(11) NOT NULL,
  `s_actuator_fk` int(11) NOT NULL,
  UNIQUE KEY `UK_2s6h090oj7y10lrgt00j9aj7r` (`s_actuator_fk`),
  KEY `FK_flfeljmblbysj3v22j7ypbqj0` (`s_gateway_fk`),
  CONSTRAINT `FK_2s6h090oj7y10lrgt00j9aj7r` FOREIGN KEY (`s_actuator_fk`) REFERENCES `s_actuator` (`id`),
  CONSTRAINT `FK_flfeljmblbysj3v22j7ypbqj0` FOREIGN KEY (`s_gateway_fk`) REFERENCES `s_device` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `s_joined_user_roles` (
  `s_user_fk` int(11) NOT NULL,
  `s_role_fk` int(11) NOT NULL,
  PRIMARY KEY (`s_user_fk`,`s_role_fk`),
  UNIQUE KEY `UK_lsk9lv3b2r5c82krrd5642gij` (`s_role_fk`),
  CONSTRAINT `FK_5gc3ippj8eduw9od31fy0wqre` FOREIGN KEY (`s_user_fk`) REFERENCES `s_users` (`id`),
  CONSTRAINT `FK_lsk9lv3b2r5c82krrd5642gij` FOREIGN KEY (`s_role_fk`) REFERENCES `s_user_roles` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `s_joined_gateway_devices` (
  `s_gateway_fk` int(11) NOT NULL,
  `s_device_fk` int(11) NOT NULL,
  UNIQUE KEY `UK_nx63imspdba29sy9dbq458rhc` (`s_device_fk`),
  KEY `FK_chshs0bxcnv23fr8ska84gf8u` (`s_gateway_fk`),
  CONSTRAINT `FK_chshs0bxcnv23fr8ska84gf8u` FOREIGN KEY (`s_gateway_fk`) REFERENCES `s_gateways` (`id`),
  CONSTRAINT `FK_nx63imspdba29sy9dbq458rhc` FOREIGN KEY (`s_device_fk`) REFERENCES `s_device` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `s_joined_device_sensors` (
  `s_gateway_fk` int(11) NOT NULL,
  `s_sensor_fk` int(11) NOT NULL,
  UNIQUE KEY `UK_gsbsunarlh4rux2x48waew9g1` (`s_sensor_fk`),
  KEY `FK_qqcemd4lsc5dbu0i7ofo9ejec` (`s_gateway_fk`),
  CONSTRAINT `FK_gsbsunarlh4rux2x48waew9g1` FOREIGN KEY (`s_sensor_fk`) REFERENCES `s_sensor` (`id`),
  CONSTRAINT `FK_qqcemd4lsc5dbu0i7ofo9ejec` FOREIGN KEY (`s_gateway_fk`) REFERENCES `s_device` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `hibernate_sequences` (
  `sequence_name` varchar(255) DEFAULT NULL,
  `sequence_next_hi_value` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
