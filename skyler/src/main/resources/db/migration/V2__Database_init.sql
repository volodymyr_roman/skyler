INSERT INTO `skyler_db`.`s_users`
(`s_email`,
`s_enabled`,
`s_first_name`,
`s_last_name`,
`s_password`,
`s_phone_number`,
`s_recovery_email`)
VALUES
('skyler@i.ua',
1,
'Skyler',
'Smart Home',
'$2a$10$Yb32VqEXXppn1xuSrhOZnuEk/YRQbPlRGvSMt2lV7taAKgQXQQVrO',
'+380668887888',
'skylerrecovery@i.ua');

INSERT INTO `skyler_db`.`s_user_roles`
(`s_role`)
VALUES
('ADMIN');
SELECT * FROM skyler_db.s_user_roles;

INSERT INTO `skyler_db`.`s_joined_user_roles`
(`s_user_fk`,
`s_role_fk`)
VALUES
(1,1);