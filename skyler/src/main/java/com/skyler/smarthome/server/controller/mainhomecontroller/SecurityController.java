package com.skyler.smarthome.server.controller.mainhomecontroller;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.rememberme.PersistentTokenBasedRememberMeServices;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@Controller
public class SecurityController {

    final static Logger logger = Logger.getLogger(SecurityController.class);

    @Autowired
    PersistentTokenBasedRememberMeServices persistentTokenBasedRememberMeServices;

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String login() {
        return "/resources/static/pages/login.html";
    }

    @RequestMapping(value = "/logout", method = RequestMethod.POST)
    public String logout(HttpServletRequest request, HttpServletResponse response) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        HttpSession session= request.getSession(false);
        if (auth != null){
            persistentTokenBasedRememberMeServices.logout(request, response, auth);
            SecurityContextHolder.getContext().setAuthentication(null);
            SecurityContextHolder.clearContext();
        }
        if (session != null){
            session.invalidate();
        }
        for(Cookie cookie : request.getCookies()) {
            cookie.setMaxAge(0);
        }
        return "redirect:/login?logout";
    }


    @RequestMapping(value = "/invalidSession", method = RequestMethod.GET)
    public String invalidSession(HttpServletRequest request, HttpServletResponse response) {
        return " ";
    }

    @RequestMapping(value = "/sessionError", method = RequestMethod.GET)
    public String sessionError(HttpServletRequest request, HttpServletResponse response) {
        return " ";
    }


}
