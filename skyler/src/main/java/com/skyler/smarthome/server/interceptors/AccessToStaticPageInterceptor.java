package com.skyler.smarthome.server.interceptors;

import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class AccessToStaticPageInterceptor extends HandlerInterceptorAdapter {

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {

        if(SecurityContextHolder.getContext().getAuthentication()
                instanceof AnonymousAuthenticationToken & request.getRequestURI().contains("html")){
            response.sendRedirect("/login");
            return false;
        }
        return true;
    }
}
