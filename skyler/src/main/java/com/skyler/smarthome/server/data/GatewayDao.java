package com.skyler.smarthome.server.data;

import com.skyler.smarthome.server.model.Gateway;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface GatewayDao {

	@PreAuthorize("hasAnyAuthority('ADMIN','USER')")
	List<Gateway> getAllGateways();

	@PreAuthorize("hasAnyAuthority('ADMIN','USER')")
	Gateway getGatewayById(int id);

	@PreAuthorize("hasAuthority('ADMIN') or isAnonymous()")
	boolean deleteGateway(int id);

	@PreAuthorize("hasAuthority('ADMIN') or isAnonymous()")
	boolean updateGatewayByField(int gatewayId, String gatewayField, String newParam);

	@PreAuthorize("hasAuthority('ADMIN') or isAnonymous()")
	boolean updateGateway(Gateway gateway);

	@PreAuthorize("hasAuthority('ADMIN') or isAnonymous()")
	boolean createGateway(Gateway gateway);
}
