package com.skyler.smarthome.server.controller.gwcontroller;

import com.skyler.smarthome.server.model.Gateway;
import com.skyler.smarthome.server.model.Module;
import com.skyler.smarthome.server.model.gateway.GatewayInfo;
import com.skyler.smarthome.server.model.gateway.MainServerInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.log4j.Logger;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

/**
 * Main internal Gateway Push controller which use to provide ability
 * register,delete,update Gateway and Modules, or change their status.
 * 
 * @see Gateway
 * @see Module
 * 
 * @author Oleksandr Marchenko
 * @version %I%, %G%
 */
@Controller
@RequestMapping("/internal")
@Api(value = "Gateway Internal Receive controller", description = "Endpoint for gateway management")
public class GatewayInternalReceiveController {

	final static Logger logger = Logger.getLogger(GatewayInternalSendController.class);

	@RequestMapping(value = "/info/", method = RequestMethod.GET,produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiOperation(value = "Get Main Server Info", notes = "Returns server protocol major and minor version", response = MainServerInfo.class)
	public @ResponseBody MainServerInfo serverInfo()
	{
		MainServerInfo mainServerInfo = new MainServerInfo();
        //TODO
		mainServerInfo.setId(1);
		mainServerInfo.setProtocolVersionMajor("1212");

        return mainServerInfo;
	}


	@RequestMapping(value = "/gateways/{unique_gateway_id}", method = RequestMethod.POST)
	@ApiOperation(value = "Register new Gateway (or initialize after system restart)")
	public void newGateway(@PathVariable int unique_gateway_id,@RequestBody GatewayInfo gatewayInfo) {
		if(unique_gateway_id > 0) {

		}
	}

	@RequestMapping(value = "/gateways/{unique_gateway_id}", method = RequestMethod.DELETE)
	@ApiOperation(value = "Delete gateway")
	public void deleteGateway(@PathVariable int unique_gateway_id) {
		if(unique_gateway_id > 0) {
			//TODO
		}
	}

	@RequestMapping(value = "/gateways/{unique_gateway_id}", method = RequestMethod.PATCH)
	@ApiOperation(value = "Update GateWay info")
	public void updateGateway(@PathVariable int unique_gateway_id, @RequestParam String gatewayField,
			@RequestParam String newParam) {
		if (unique_gateway_id > 0&& gatewayField != null && newParam != null) {
			//TODO
		}
	}

	@RequestMapping(value = "/gateways/{unique_gateway_id}/sensors/sys-event/", method = RequestMethod.POST,produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiOperation(value = "Register system event happened with some of the Sensors (new added, communication lost, missing, poor communication, etc)")
	public @ResponseBody boolean registerSensorSystemEvent(@PathVariable int unique_gateway_id) {
		//TODO
		return true;
	}

	@RequestMapping(value = "/gateways/{unique_gateway_id}/sensors/{sensor_id}/event/", method = RequestMethod.POST,produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiOperation(value = "Register event happened on Sensor")
	public @ResponseBody boolean registerSensorEvent(@PathVariable int unique_gateway_id, @RequestParam int sensor_id) {
		//TODO
		return true;
	}

	@RequestMapping(value = "/gateways/{unique_gateway_id}/actuators/sys-event/", method = RequestMethod.POST,produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiOperation(value = "Register system event happened with some of the Actuators (new added, communication lost, missing, poor communication, etc)")
	public @ResponseBody boolean registerActuatorSystemEvent(@PathVariable int unique_gateway_id) {
		//TODO
		return true;
	}

	@RequestMapping(value = "/gateways/{unique_gateway_id}/sensors/", method = RequestMethod.POST,produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiOperation(value = "Register new sensor")
	public @ResponseBody boolean registerNewSensor(@PathVariable int unique_gateway_id) {
		//TODO
		return true;
	}

	@RequestMapping(value = "/gateways/{unique_gateway_id}/actuators/", method = RequestMethod.POST,produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiOperation(value = "Register new actuator")
	public @ResponseBody boolean registerNewActuator(@PathVariable int unique_gateway_id) {
		//TODO
		return true;
	}
}
