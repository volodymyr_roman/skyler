package com.skyler.smarthome.server.data;

import com.skyler.smarthome.server.model.Device;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DeviceDao {

    @PreAuthorize("hasAuthority('ADMIN') or isAnonymous()")
    public List<Device> getAllDevices();

    @PreAuthorize("hasAuthority('ADMIN') or isAnonymous()")
    public List<Device> getAllDevicesFromGateway(int gwId);

    @PreAuthorize("hasAuthority('ADMIN') or isAnonymous()")
    public Device getDeviceById(int id);

    @PreAuthorize("hasAuthority('ADMIN') or isAnonymous()")
    public boolean addDeviceToGateway(int gateway, Device device);

    @PreAuthorize("hasAuthority('ADMIN') or isAnonymous()")
    public boolean addDeviceListToGateway(int gateway, List<Device> devicesList);
}
