package com.skyler.smarthome.server.data;

import com.skyler.smarthome.server.model.UserRole;
import org.springframework.security.access.prepost.PreAuthorize;

import java.util.Set;

public interface UserRoleDao {

    @PreAuthorize("hasAuthority('ADMIN')")
    Set<UserRole> getAllRoles();

    @PreAuthorize("hasAuthority('ADMIN')")
    UserRole getRoleById(int id);

}
