package com.skyler.smarthome.server.data;

import com.skyler.smarthome.server.model.Actuator;
import com.skyler.smarthome.server.model.Module;
import com.skyler.smarthome.server.model.Sensor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ModuleDao {

	@PreAuthorize("hasAuthority('ADMIN') or isAnonymous()")
	public List<Module> getAllModules();

	@PreAuthorize("hasAuthority('ADMIN') or isAnonymous()")
    public List<Sensor> getAllSensor();

	@PreAuthorize("hasAuthority('ADMIN') or isAnonymous()")
    public List<Actuator> getAllActuator();

	@PreAuthorize("hasAuthority('ADMIN') or isAnonymous()")
	public Sensor getSensorById(int sensorId);

	@PreAuthorize("hasAuthority('ADMIN') or isAnonymous()")
    public Actuator getActuatorById(int actuatorId);

	@PreAuthorize("hasAuthority('ADMIN') or isAnonymous()")
	public boolean addModuleToDevice(int gatewayId, int deviceId, Module module);

	@PreAuthorize("hasAuthority('ADMIN') or isAnonymous()")
	public boolean addSensorListToDevice(int gatewayId,int deviceId, List<Sensor> sensorList);

	@PreAuthorize("hasAuthority('ADMIN') or isAnonymous()")
	public boolean addActuatorListToDevice(int gatewayId,int deviceId, List<Actuator> actuatorList);

}
